
ifndef CFG
CFG = Debug
endif

OUTDIR = $(CFG)

CSOURCES = dsd.c memmap.c binloader.c mips.c tables.c int.c dbg.c intcop.c intfpu.c intvfpu.c vfpuutil.c

INCPATHS = -I. -Icpu

WINFLAGS =  -g -Wall
CFLAGS = $(WINFLAGS) $(INCPATHS)
#CXXFLAGS = $(WINFLAGS) -I/usr/include/SDL
#SDLLIBS = $(shell sdl-config --libs)
#LDFLAGS = $(WINFLAGS) $(SDLLIBS) -lopengl32 -lglu32
LDFLAGS = $(WINFLAGS)
#OBJFILES = $(CXXSOURCES:%.cpp=$(OUTDIR)/%.o)
OBJFILES = $(CSOURCES:%.c=$(OUTDIR)/%.o)


OUTPUT = $(OUTDIR)/dsd.exe

vpath %.c cpu

#CC = i686-pc-mingw32-gcc
#CXX = i686-pc-mingw32-g++
#LD = i686-pc-mingw32-gcc

CC = i686-pc-cygwin-gcc
CXX = i686-pc-cygwin-g++
LD = i686-pc-cygwin-gcc

MKDIR = mkdir

DEPPAT = $(OUTDIR)/$(*F)

all: $(OUTPUT)
	cp $(OUTPUT) .

$(OUTPUT): $(OUTDIR) $(OBJFILES)
	$(LD) $(OBJFILES) $(LDFLAGS) -o "$(OUTPUT)"

$(OUTDIR)/%.o: %.c
# stdafx.h.gch
	$(CC) $(CFLAGS) -MD -c -o $@ $<
	@cp $(DEPPAT).d $(DEPPAT).dep; \
		sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
		-e '/^$$/ d' -e 's/$$/ :/' < $(DEPPAT).d >> $(DEPPAT).dep; \
		rm -f $(DEPPAT).d

$(OUTDIR):
	$(MKDIR) "$(OUTDIR)"
	
#stdafx.h.gch: stdafx.h
#	$(CXX) $(CXXFLAGS) $<

clean:
	rm -rf $(OUTDIR)
	rm -f *.gch
	
#-include $(CXXSOURCES:%.cpp=$(OUTDIR)/%.dep)
-include $(CSOURCES:%.c=$(OUTDIR)/%.dep)

