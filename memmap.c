
#include <sys/mman.h>
#include "memmap.h"
#include "logger.h"


static void *sBase;

extern void *image;

u32 mem_read32(u32 addr)
{
	return *(u32*)(image + addr);
}

u16 mem_read16(u32 addr)
{
	return *(u16*)(image + addr);
}

u8 mem_read8(u32 addr)
{
	return *(u8*)(image + addr);
}

float mem_readFloat(u32 addr)
{
	return *(float*)(image + addr);
}

void mem_write32(u32 addr, u32 val)
{
	*(u32*)(image + addr) = val;
}

void mem_write16(u32 addr, u16 val)
{
	*(u16*)(image + addr) = val;
}

void mem_write8(u32 addr, u8 val)
{
	*(u8*)(image + addr) = val;
}

void mem_writeFloat(u32 addr, float val)
{
	*(float*)(image + addr) = val;
}

int mem_isOk(u32 addr)
{
	return 1;
}


typedef struct
{
	u32		start;
	u32		end;
} TMemRegion;


/*
       start        end     size 	description
	0x00010000 	0x00013fff 	16kb 	scratchpad
	0x04000000 	0x041fffff 	2mb 	Video Memory / Frame Buffer
	0x08000000 	0x09ffffff 	32mb 	Main Memory
	0x1c000000 	0x1fbfffff 	  		Hardware i/o
	0x1fc00000 	0x1fcfffff 	1mb 	Hardware Exception Vectors (RAM)
	0x1fd00000 	0x1fffffff 	  		Hardware i/o
*/

#define MEM_SCRATCHPAD_START	0x00010000
#define MEM_SCRATCHPAD_SIZE		0x00004000
#define MEM_SCRATCHPAD_END		(MEM_SCRATCHPAD_START + MEM_SCRATCHPAD_SIZE)

#define MEM_VMEM_START			0x04000000
#define MEM_VMEM_SIZE			0x00200000
#define MEM_VMEM_END			(MEM_VMEM_START + MEM_VMEM_SIZE)

#define MEM_MAINMEM_START		0x08000000
#define MEM_MAINMEM_SIZE		0x02000000
#define MEM_MAINMEM_END			(MEM_MAINMEM_START + MEM_MAINMEM_SIZE)

#define VIRTMEM_START			0x00000000
#define VIRTMEM_END				MEM_MAINMEM_END
#define VIRTMEM_SIZE			(VIRTMEM_END - VIRTMEM_START)

static const TMemRegion sMemMap[] = 
{
	{ MEM_SCRATCHPAD_START, MEM_SCRATCHPAD_END },
	{ MEM_VMEM_START, MEM_VMEM_END },
	{ MEM_MAINMEM_START, MEM_MAINMEM_END }
};

#define MEM_REGIONS_CNT			(sizeof(sMemMap) / sizeof(TMemRegion))

s32 memInit()
{
	int				result;
	int				i;

	// to RESERVE memory in Linux, use mmap with a private, anonymous, non-accessible mapping.
	// The following line reserves 1gb of ram starting at 0x10000000.
	
	sBase = mmap(VIRTMEM_START, VIRTMEM_SIZE, PROT_NONE, MAP_PRIVATE | MAP_ANON, -1, 0);

	dbg("Reserved 0x%X bytes from 0x%X", VIRTMEM_SIZE, sBase);

	if(sBase == NULL)
	{
		dbg(" * ERROR: Cannot reserve virtual memory address space!");
		return -1;
	}
	
	// to COMMIT memory in Linux, use mprotect on the range of memory you'd like to commit, and
	// grant the memory READ and/or WRITE access.
	// The following line commits 1mb of the buffer.  It will return -1 on out of memory errors.
	
	for(i = 0; i < MEM_REGIONS_CNT; i++)
	{
		result = mprotect(sBase + sMemMap[i].start, sMemMap[i].end - sMemMap[i].start, PROT_READ | PROT_WRITE);
		if(result != 0)
		{
			dbg(" * ERROR: Cannot commit region 0x%X - 0x%X to 0x%X",
				sMemMap[i].start,
				sMemMap[i].end,
				sBase + sMemMap[i].start);

			break;
		}
	}

	if(result != 0)
	{
		munmap(sBase, VIRTMEM_SIZE);
	}

	return result;
}

s32 memTerm()
{
	if(sBase != NULL)
	{
		munmap(sBase, VIRTMEM_SIZE);
	}

	return 0;
}

void* memMapGuest(u32 guestAddr)
{
	return sBase + guestAddr;
}

u32 memMapHost(u32 hostAddr)
{
	if(hostAddr <= sBase)
		return 0;

	return hostAddr - (u32)sBase;
}
