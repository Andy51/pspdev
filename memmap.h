#ifndef _MEMMAP__H
#define _MEMMAP__H

#include "typedefs.h"

s32 memInit();
s32 memTerm();

u32 mem_read32(u32 addr);
u16 mem_read16(u32 addr);
u8 mem_read8(u32 addr);
float mem_readFloat(u32 addr);

void mem_write32(u32 addr, u32 val);
void mem_write16(u32 addr, u16 val);
void mem_write8(u32 addr, u8 val);
void mem_writeFloat(u32 addr, float val);

int mem_isOk(u32 addr);

void* memMapGuest(u32 guestAddr);
u32 memMapHost(u32 hostAddr);

#endif

