#ifndef _LOGGER__H
#define _LOGGER__H

#include <stdio.h>

#define _log(_lvl_, _mod_, _fmt_, ...) \
	printf(_fmt_"\n", ## __VA_ARGS__ )
	

#define dbg(_fmt_, ...) \
	printf(_fmt_"\n", ## __VA_ARGS__ )

#endif

