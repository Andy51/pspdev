#ifndef TYPEDEFS__H
#define TYPEDEFS__H


typedef signed int			s32;
typedef unsigned int		u32;
typedef signed short		s16;
typedef unsigned short		u16;
typedef signed char			s8;
typedef unsigned char		u8;

#ifdef __GNUC__
typedef signed long long	s64;
typedef unsigned long long	u64;
#else
#error Need to define 64 bit types for the compiler
#endif


static __inline u32 E32( u32 x )
{
	#ifdef __GNUC__ /* GCC syntax */

		__asm(	".intel_syntax noprefix\n"
				//"mov	eax, [x]	\n"
				"bswap	eax		\n"
				//"mov	[x], eax	\n"
				".att_syntax noprefix\n"
				: "=a" (x)		// eax = x
				: "a" (x)
		);

	#else /* VS syntax */

		__asm {
			mov	eax, x
			bswap eax
			mov	 x, eax
		};
	#endif

	return x;
}

static __inline s16 E16( s16 x )
{
	#ifdef __GNUC__ /* GCC syntax */

		__asm(	".intel_syntax noprefix\n"
				"xchg	ah, al		\n"
				".att_syntax noprefix\n"
				: "=a" (x)		// eax = x
				: "a" (x)
		);

	#else /* VS syntax */

		__asm {
			xor  eax, eax
			mov	 ax, x
			xchg ah,al
			mov  x, ax
		}

	#endif

	return x;
}



#endif
