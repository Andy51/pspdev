
#include "elfloader.h"


s32 loadElf(char *path, u32 *addr)
{
	TElf			telf;
	s32				result = -1;

	dbg("Loading the elf %s", path);

	memset(&telf, 0, sizeof(telf));

	telf.f = fopen(path, "rb");
	if(telf.f == NULL)
		return result;

	result = loadHeader(&telf);
	if(result != 0)
		goto ret;

	result = loadProgram(&telf);
	if(result != 0)
		goto ret;

	result = relocate(&telf);
	if(result != 0)
		goto ret;

ret:
	freeElf(&elf);

	if(telf.addr != 0)
	{
		*addr = telf.addr;
	}

	return result;
}

static s32 loadHeader(TElf *elf)
{
	Elf32_Ehdr		*ehdr = &elf->ehdr;

	fread(&ehdr, sizeof(Elf32_Ehdr), 1, elf->f);

	if( *((u32*)ehdr->e_ident) != ELFSIGNATURE || 
		ehdr->e_ident[4] != ELFCLASS32 ||
		ehdr->e_ident[5] != ELFDATA2LSB ||
		ehdr->e_ident[6] != EV_CURRENT ||
		ehdr->e_machine != EM_MIPS )
	{
		return -1;
	}

	dbg("Header OK");

	elf->phdr = (Elf32_Phdr*)malloc(sizeof(Elf32_Phdr) * ehdr->e_phnum);
	if(elf->phdr == NULL)
	{
		return -1;
	}

	fseek(f, ehdr->e_phoff, SEEK_SET);

	fread(elf->phdr, sizeof(Elf32_Phdr), ehdr->e_phnum, elf->f);

	return 0;
}


static s32 loadProgram(TElf *elf)
{
	Elf32_Ehdr			*ehdr = &elf->ehdr;
	Elf32_Phdr			*phdr = &elf->phdr;
	void				*mem;
	s32					i;


	dbg("Loading the image...");
	
	for(i = 0; i < ehdr->e_phnum; i++)
	{
		dbg("PHDR: type %d off 0x%X vaddr 0x%X memsz %d filesz %d",
			phdr[i].p_type,
			phdr[i].p_offset,
			phdr[i].p_vaddr,
			phdr[i].p_memsz,
			phdr[i].p_filesz);
		
		if(phdr[i].p_type == PT_LOAD)
		{
			mem = memMapGuest(phdr[i].p_vaddr);
			if(mem == NULL)
				return -1;

			/* Read the image from the file */
			fseek(f, phdr[i].p_offset, SEEK_SET);
			fread(mem, phdr[i].p_filesz, 1, f);

			/* Clear the BSS */
			if(phdr[i].p_memsz != phdr[i].p_filesz)
			{
				memset(mem + phdr[i].p_filesz, 0, phdr[i].p_memsz - phdr[i].p_filesz);
			}

			dbg("  \- loaded at 0x%X", mem);

			elf->addr = phdr[i].p_vaddr;
		}
	}

	dbg("Done loading");

	return 0;
}

static s32 relocate(TElf *elf)
{
	dbg("Relocations are not implemented yet...")

	return 0;
}

static s32 freeElf(TElf *elf)
{
	if(elf->phdr != NULL)
		free(elf->phdr);

	if(elf->f != NULL)
		fclose(telf.f);
}


