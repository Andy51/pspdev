#ifndef _BINLOADER__H
#define _BINLOADER__H

#include "typedefs.h"

u32 loadBIN(const char *path, void **buf);

#endif

