
#include <stdio.h>
#include <stdlib.h>
#include "binloader.h"
#include "typedefs.h"

u32	getFileSize(FILE* f)
{
	u32				sz;

	fseek(f, 0, SEEK_END);
	sz = ftell(f);
	fseek(f, 0, SEEK_SET);

	return sz;
}

s32 loadBIN(const char *path, void **buf)
{
	FILE		*f;
	u32			sz;
	
	if(buf == NULL)
		return -1;
	
	f = fopen(path, "rb");
	if(f == NULL)
		return -1;
	
	sz = getFileSize(f);
	if(sz == 0)
		return -1;
		
	*buf = malloc(sz);
		
	fread(*buf, 1, sz, f);	
	
	fclose(f);
	
	return 0;
}

