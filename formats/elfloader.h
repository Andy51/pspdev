#ifndef ELFLOADER__H
#define ELFLOADER__H

#include "typedefs.h"
#include "elf.h"

typedef struct
{
	FILE			*f;
	u32				addr;
	Elf32_Ehdr		ehdr;
	Elf32_Phdr		*phdr;
} TElf;


s32 loadElf(char *path, u32 *addr);

#endif // ELFLOADER__H
