#ifndef INTCOP_H
#define INTCOP_H

void int_mfc0(u32 op);
void int_mtc0(u32 op);
void int_cfc0(u32 op);
void int_ctc0(u32 op);

#endif
