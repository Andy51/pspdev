#ifndef DBG_H
#define DBG_H

#define DESCDEF(fn) void fn(char *out, u32 len, u32 op)

DESCDEF(dbg_jal);
DESCDEF(dbg_jalr);
DESCDEF(dbg_j);
DESCDEF(dbg_jr);
DESCDEF(dbg_syscall);
DESCDEF(dbg_beq);
DESCDEF(dbg_bne);
DESCDEF(dbg_blez);
DESCDEF(dbg_beql);
DESCDEF(dbg_bnel);
DESCDEF(dbg_bgez);
DESCDEF(dbg_bgezl);
DESCDEF(dbg_addi);
DESCDEF(dbg_addiu);
DESCDEF(dbg_andi);
DESCDEF(dbg_ori);
DESCDEF(dbg_lui);
DESCDEF(dbg_add);
DESCDEF(dbg_addu);
DESCDEF(dbg_and);
DESCDEF(dbg_or);
DESCDEF(dbg_slt);
DESCDEF(dbg_sltu);
DESCDEF(dbg_lw);
DESCDEF(dbg_lwc1);
DESCDEF(dbg_lwl);
DESCDEF(dbg_lwr);
DESCDEF(dbg_lb);
DESCDEF(dbg_lbu);
DESCDEF(dbg_lh);
DESCDEF(dbg_lhu);
DESCDEF(dbg_sb);
DESCDEF(dbg_sw);
DESCDEF(dbg_swc1);
DESCDEF(dbg_swl);
DESCDEF(dbg_swr);
DESCDEF(dbg_sh);
DESCDEF(dbg_sll);
DESCDEF(dbg_abs);
DESCDEF(dbg_fadd);
DESCDEF(dbg_fsub);
DESCDEF(dbg_fmul);
DESCDEF(dbg_fdiv);
DESCDEF(dbg_mov);
DESCDEF(dbg_bgtz);
DESCDEF(dbg_bgtzl);
DESCDEF(dbg_xor);
DESCDEF(dbg_xori);
DESCDEF(dbg_slti);
DESCDEF(dbg_sltiu);
DESCDEF(dbg_blezl);
DESCDEF(dbg_cache);
DESCDEF(dbg_bltz);
DESCDEF(dbg_bltzl);
DESCDEF(dbg_movz);
DESCDEF(dbg_sra);
DESCDEF(dbg_pref);
DESCDEF(dbg_sllv);
DESCDEF(dbg_srl);
DESCDEF(dbg_srlv);
DESCDEF(dbg_rotr);
DESCDEF(dbg_rotrv);
DESCDEF(dbg_srav);
DESCDEF(dbg_movn);
DESCDEF(dbg_mthi);
DESCDEF(dbg_mfhi);
DESCDEF(dbg_mtlo);
DESCDEF(dbg_mflo);
DESCDEF(dbg_nor);
DESCDEF(dbg_mult);
DESCDEF(dbg_multu);
DESCDEF(dbg_div);
DESCDEF(dbg_divu);
DESCDEF(dbg_sub);
DESCDEF(dbg_subu);
DESCDEF(dbg_break);
DESCDEF(dbg_min);
DESCDEF(dbg_max);
DESCDEF(dbg_seb);
DESCDEF(dbg_seh);
DESCDEF(dbg_ins);
DESCDEF(dbg_ext);
DESCDEF(dbg_clz);
DESCDEF(dbg_clo);
DESCDEF(dbg_madd);
DESCDEF(dbg_maddu);
DESCDEF(dbg_msub);
DESCDEF(dbg_msubu);
DESCDEF(dbg_mfic);
DESCDEF(dbg_mtic);
DESCDEF(dbg_mfv);
DESCDEF(dbg_mtv);
DESCDEF(dbg_bvf);
DESCDEF(dbg_bvfl);
DESCDEF(dbg_bvt);
DESCDEF(dbg_bvtl);
DESCDEF(dbg_mfc0);
DESCDEF(dbg_mtc0);
DESCDEF(dbg_cfc0);
DESCDEF(dbg_ctc0);
DESCDEF(dbg_mfc1);
DESCDEF(dbg_mtc1);
DESCDEF(dbg_cfc1);
DESCDEF(dbg_ctc1);
DESCDEF(dbg_bc1f);
DESCDEF(dbg_bc1fl);
DESCDEF(dbg_bc1t);
DESCDEF(dbg_bc1tl);
DESCDEF(dbg_sqrt);
DESCDEF(dbg_neg);
DESCDEF(dbg_round);
DESCDEF(dbg_trunc);
DESCDEF(dbg_ceil);
DESCDEF(dbg_floor);
DESCDEF(dbg_cvtsw);
DESCDEF(dbg_cvtws);
DESCDEF(dbg_eq);
DESCDEF(dbg_lt);
DESCDEF(dbg_nge);
DESCDEF(dbg_le);
DESCDEF(dbg_ngt);
DESCDEF(dbg_wsbh);
DESCDEF(dbg_wsbw);
DESCDEF(dbg_bitrev);

#endif
