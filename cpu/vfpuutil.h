#ifndef VFPUUTILS_H
#define VFPUUTILS_H

#include "typedefs.h"

typedef enum
{
    V_Single,
    V_Pair,
    V_Triple,
    V_Quad,
    V_2x2,
    V_3x3,
    V_4x4,
    V_Invalid
} DataSize;

#define _VD (op & 0x7f)
#define _VS ((op >> 8) & 0x7f)
#define _VT ((op >> 16) & 0x7f)

#define Xpose(v) v ^ 0x20

#define VFPU_FLOAT16_EXP_MAX    0x1f
#define VFPU_SH_FLOAT16_SIGN    15
#define VFPU_MASK_FLOAT16_SIGN    0x1
#define VFPU_SH_FLOAT16_EXP    10
#define VFPU_MASK_FLOAT16_EXP    0x1f
#define VFPU_SH_FLOAT16_FRAC    0
#define VFPU_MASK_FLOAT16_FRAC    0x3ff

void ReadMatrix(float *rd, DataSize size, s32 outMatrixWidth, s32 reg);
void WriteMatrix(const float *rs, DataSize size, s32 inMatrixWidth, s32 reg);

#define ReadVector(rs, N, reg) ReadMatrix(rs, N, 0, reg)
#define WriteVector(rs, N, reg) WriteMatrix(rs, N, 0, reg)

void SetWriteMask(s32 wm[4]);
DataSize GetVecSize(u32 op);
DataSize GetMtxSize(u32 op);
DataSize GetHalfSize(DataSize sz);
s32 GetNumElements(DataSize sz);
s32 GetMatrixSide(DataSize sz);

float Float16ToFloat32(u16 l);

#endif

