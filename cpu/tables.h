#ifndef MIPSTABLES_H
#define MIPSTABLES_H

#include "typedefs.h"

typedef void (*IntFunc)(u32 opcode);
typedef void (*DbgFunc)(char *out, u32 len, u32 opcode);

void cpu_interpret(u32 op);
void cpu_debug(char *string, u32 len, u32 op);

#endif

