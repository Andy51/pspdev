#ifndef INTFPU_H
#define INTFPU_H

void int_lwc1(u32 op);
void int_swc1(u32 op);
void int_mfc1(u32 op);
void int_cfc1(u32 op);
void int_mtc1(u32 op);
void int_ctc1(u32 op);
void int_sqrt(u32 op);
void int_abs(u32 op);
void int_mov(u32 op);
void int_neg(u32 op);
void int_round(u32 op);
void int_trunc(u32 op);
void int_ceil(u32 op);
void int_floor(u32 op);
void int_cvtsw(u32 op);
void int_cvtws(u32 op);
void int_eq(u32 op);
void int_lt(u32 op);
void int_nge(u32 op);
void int_le(u32 op);
void int_ngt(u32 op);
void int_fadd(u32 op);
void int_fsub(u32 op);
void int_fmul(u32 op);
void int_fdiv(u32 op);
void int_bc1f(u32 op);
void int_bc1t(u32 op);
void int_bc1fl(u32 op);
void int_bc1tl(u32 op);

#endif
