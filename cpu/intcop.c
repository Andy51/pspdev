#include "mips.h"
#include "logger.h"

#define C0ST(i) cpu.cop0st[i]
#define C0CTL(i) cpu.cop0ctl[i]

void int_mfc0(u32 op)
{
    R(_RT) = C0ST(_RD);
    _log(INF, CPU, "Loading %08x from state register %d", R(_RT), _RD);
}

void int_mtc0(u32 op)
{
    C0ST(_RD) = R(_RT);
    _log(INF, CPU, "Writing %08x in state register %d", C0ST(_RD), _RD);
}

void int_cfc0(u32 op)
{
    R(_RT) = C0CTL(_RD);
}

void int_ctc0(u32 op)
{
    C0CTL(_RD) = R(_RT);
}

